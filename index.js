var MailParser = require("mailparser").MailParser;
var Mbox = require('node-mbox');
var mbox = new Mbox();


var participants = [];

// Next, catch events generated:
mbox.on('message', function(msg) {
	// console.log('got a message', msg);
	// parse message using MailParser
  var mailparser = new MailParser({ streamAttachments : true });
  mailparser.on('headers', function(headers) {
    if (participants.indexOf(headers.from) === -1) {
      if (headers.from.indexOf("island.byu.edu") === -1) {
        participants.push(headers.from);  
      }
    }
    // console.log('From   :', headers.from);
    // console.log('Subject:', headers.subject, '\n');
  });
  mailparser.write(msg);
  mailparser.end();
});

mbox.on('error', function(err) {
  console.log('got an error', err);
});

mbox.on('end', function() {
  console.log('done reading mbox file');
  console.log('All participants:');

  participants.forEach(function (contact) {
    console.log(contact.replace(' <', ',').replace('>', ''));
  })
});

process.stdin.pipe(mbox);