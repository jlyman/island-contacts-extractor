# Usage

Run from the command line, piping in an MBOX file, and for convenience, pipe it out to a file like this:

    node index.js < /path/to/my.mbox >> extractedParticipants.csv